package main

import (
	"context"

	// _ "gitee.com/go-kade/library/ioc/config/datasource"  初始化时做过了
	"gitee.com/go-kade/library/ioc"
	_ "gitee.com/go-kade/sso/app/token/api"
	_ "gitee.com/go-kade/sso/app/user/api"
	"gitee.com/go-kade/sso/application"
)

func main() {
	req := ioc.NewLoadConfigRequest()
	req.ConfigFile.Enabled = true
	req.ConfigFile.Path = "etc/application.toml"
	ioc.DevelopmentSetup(req)
	// a := ioc.Default().Get(impl.AppName).(*impl.UserServiceImpl)
	// req1 := user.NewCreateUserRequest()
	// req1.UserName = "sja11"
	// req1.PassWord = "sman11"
	// req1.Label = map[string]string{"bsa1b": "111111"}
	// t.Log(a.CreateUser(context.Background(), req1))
	// 加载 Gin API，指定路径前缀和路由器对象
	// r := gin.Default()
	// // // a := r.Group("ccc")
	// // r.POST("/", func(c *gin.Context) {
	// // 	c.JSON(200, gin.H{"message": "Hello from Gin API"})
	// // })
	// r.LoadHTMLGlob("views/*") //配置模版中间件
	// ioc.LoadGinApi("/api/v1", r)
	// // 启动 Gin 服务器
	// r.Run(":8080")

	m := application.App()
	m.HTTP.Start(context.Background())
}
