package exception_test

import (
	"fmt"
	"testing"

	"gitee.com/go-kade/sso/exception"
)

func TestNewError(t *testing.T) {
	e := exception.NewNotFound("test %s", "ss")
	t.Log(e.ToJson())
	fmt.Println(e.Error())
}
