package application_test

import (
	"context"
	"testing"

	"gitee.com/go-kade/library/ioc"
	"gitee.com/go-kade/sso/application"
)

func TestApp(t *testing.T) {
	m := application.App()
	m.HTTP.Start(context.Background())
	t.Log(m)

}

func init() {
	req := ioc.NewLoadConfigRequest()
	req.ConfigFile.Enabled = true
	req.ConfigFile.Path = "../etc/application.toml"
	ioc.DevelopmentSetup(req)
}
