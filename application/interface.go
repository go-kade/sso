package application

import (
	"gitee.com/go-kade/library/ioc"
)

const (
	AppName = "applicattion_config"
)

func App() *Application {
	return ioc.Config().Get(AppName).(*Application)
}
