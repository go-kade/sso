package application

import (
	"context"

	"gitee.com/go-kade/library/ioc"
)

type Application struct {
	AppName        string `json:"name" yaml:"name" toml:"name" env:"APP_NAME"`
	AppDescription string `json:"description" yaml:"description" toml:"description" env:"APP_DESCRIPTION"`
	EncryptKey     string `json:"encrypt_key" yaml:"encrypt_key" toml:"encrypt_key" env:"APP_ENCRYPT_KEY"`
	// CipherPrefix   string `json:"cipher_prefix" yaml:"cipher_prefix" toml:"cipher_prefix" env:"APP_CIPHER_PREFIX"`

	HTTP *Http `json:"http" yaml:"http"  toml:"http"`
	// GRPC *Grpc `json:"grpc" yaml:"grpc"  toml:"grpc"`

	// ioc.ObjectImpl

	// ch     chan os.Signal
	// log    *zerolog.Logger
	// ctx    context.Context
	// cancle context.CancelFunc
}

func init() {
	ioc.Config().Registry(&Application{
		AppName:    "mcube_app",
		EncryptKey: "defualt app encrypt key",
		// CipherPrefix: "@ciphered@",
		HTTP: NewDefaultHttp(),
		// GRPC:         NewDefaultGrpc()
	})
}

func (u *Application) Init() error {
	// u.db = datasource.DB(context.Background())
	return nil
}

func (u *Application) Name() string {
	return AppName
}

func (u *Application) Version() string {
	return ""
}

func (u *Application) Priority() int {
	return 9
}

func (u *Application) Close(ctx context.Context) error {
	return nil
}

func (u *Application) AllowOverwrite() bool {
	return true
}
